﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Soln_RazorRoundedSquare.Entity
{
    public class ProductEntity
    {
        public dynamic ProductId { get; set; }
        public dynamic ProductName { get; set; }
        public dynamic ProductNumber {get;set;}
        public dynamic ProductPrice { get; set; }
        public dynamic ProductColor { get; set; }
    }
}