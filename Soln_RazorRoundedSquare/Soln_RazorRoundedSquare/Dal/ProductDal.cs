﻿using Soln_RazorRoundedSquare.Dal.ORM;
using Soln_RazorRoundedSquare.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Soln_RazorRoundedSquare.Dal
{
    public class ProductDal
    {
          #region Declaration
        private ProductdcDataContext dc = null;
        #endregion

        #region Constructor
        public ProductDal()
        {
            dc = new ProductdcDataContext();
        }
        #endregion

        #region Public Methods
        public IEnumerable<ProductEntity> GetProductData()
        {
            return
                dc
                .Products
                .AsEnumerable()
                .Select((leLinqProductObj) => new ProductEntity()
                {
                    ProductId = leLinqProductObj.ProductID,
                    ProductName = leLinqProductObj.Name,
                    ProductNumber = leLinqProductObj.ProductNumber,
                    ProductPrice = leLinqProductObj.ListPrice,
                    ProductColor = leLinqProductObj.Color
                })
                .Take(5)
                .ToList();
        }
        #endregion  
    }
}